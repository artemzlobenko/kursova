<?php

use App\Http\Controllers\AdminTripController;
use App\Http\Controllers\TripsController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/trips', [TripsController::class, 'list']);
Route::get('/trips/{trip}', [TripsController::class, 'trip']);

Route::resource('/admin/trips', AdminTripController::class)->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
