<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Trips extends Model
{
    use HasFactory;

    public function getCities() {
        $cities = DB::table('destination')
            ->select('*')
            ->get();
        return $cities;
    }

    public function getTrips($dateFrom, $dateTo, $dest, $inStock){
        $query = DB::table('trips');
        $query->join('class', 'trips.class_id', '=', 'class.class_id')
            ->join('destination', 'trips.destination_id', '=', 'destination.city_id')
            ->select('trips.trip_id', 'class.class_type', 'destination.city_name', 'trips.date', 'trips.tickets')
            ->orderBy('trips.date');

        if($dateFrom && $dateTo){
            $query->whereBetween('trips.date', [$dateFrom, $dateTo]);
        }
        elseif($dateFrom){
            $query->where('trips.date', '>=', $dateFrom);
        }
        elseif($dateTo){
            $query->where('trips.date', '<=', $dateTo);
        }

        if($dest){
            $query->where('destination.city_id', '=', $dest);
        }

        if($inStock){
            $query->where('trips.tickets', '<', '30');
        }

        $trips = $query->get();
        return $trips;
    }

    public function getTripByID($id){
        if(!$id) return null;
        $trip = DB::table('trips')
            ->join('class', 'trips.class_id', '=', 'class.class_id')
            ->join('destination', 'trips.destination_id', '=', 'destination.city_id')
            ->select('trips.trip_id', 'class.class_type', 'destination.city_name', 'trips.date', 'trips.tickets')
            ->where('trip_id', '=', $id)
            ->get()->first();
        return $trip;
    }

}
