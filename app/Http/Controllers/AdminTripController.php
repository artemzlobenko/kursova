<?php

namespace App\Http\Controllers;

use App\Models\Trips;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AdminTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model_trips = new Trips();
        $trips = $model_trips->getTrips(null, null, null, null);
        $trip_cities = $model_trips->getCities();
        return view('admin.trip.list', ['trips' => $trips,
                'trip_cities' => $trip_cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model_trips = new Trips();

        $trip_cities = $model_trips->getCities();
        return view('admin.trip.add', ['trip_cities' => $trip_cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $classId = $request->input('class_id');
        $destinationId = $request->input('destination_id');
        $date = $request->input('date');
        $tickets = $request->input('tickets');
        DB::table('trips')->insertGetId([
            'class_id' => $classId, 'destination_id' => $destinationId, 'date' => $date, 'tickets' => $tickets
        ]);
        return redirect('/admin/trips');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trip = DB::table('trips')->where('trip_id', $id)->first();
        $dest = $trip->destination_id;
        $model_trips = new Trips();
        $trip_cities = $model_trips->getCities();
        return view('admin.trip.edit', [
            'trip' => $trip,
            'trip_cities' => $trip_cities,
            'city_selected' => $dest]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $classId = $request->input('class_id');
        $destinationId = $request->input('destination_id');
        $date = $request->input('date');
        $tickets = $request->input('tickets');

        $model_trips = new Trips();
        $trip = $model_trips->getTripByID($id);
        DB::table('trips')
            ->where('trip_id', '=', $trip->trip_id)
            ->update(['class_id' => $classId, 'destination_id' => $destinationId, 'date' => $date, 'tickets' => $tickets
            ]);

        return redirect('/admin/trips');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('trips')->where('trip_id', '=', $id)->delete();
        return redirect('/admin/trips');
    }
}
