@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Add trip</h2>
    <form action="/admin/trips" method="POST">
        {{ csrf_field() }}
        <label>Class id </label>
        <input type="number" name="class_id">
        <br/><br/>
        <label>Date </label>
        <input type="date" name="date">
        <br/><br/>
        <label>Tickets</label>
        <input type="number" name="tickets">
        <br/><br/>
        <label>Destination</label>
        <select name="destination_id">
            @foreach($trip_cities as $city)
                <option value="{{ $city->city_id }}">
                    {{ $city->city_name }}
                </option>
            @endforeach
        </select>
        <br/><br/>
        <input type="submit" value="Save">
    </form>
@endsection
