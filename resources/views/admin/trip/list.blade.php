@extends('admin.layout')
@section('content')
    <h2>Travel list</h2>
    <table border="1" style="text-align: center;">
        <th>Trip id</th>
        <th>Destination</th>
        <th>Date</th>
        <th>Tickets</th>
        <th>Action</th>
        @foreach ($trips as $trip)
            <tr>
                <td>
                    <a href="/trips/{{ $trip->trip_id }}">{{ $trip->trip_id}}</a>
                </td>
                <td>{{ $trip->city_name }}</td>
                <td>{{ $trip->date }}</td>
                <td>{{ $trip->tickets }}/30</td>
                <td>
                    <a href="/admin/trips/{{ $trip->trip_id }}/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/trips/{{ $trip->trip_id }}"method="POST">
                        {{ method_field('DELETE') }}

                        {{ csrf_field() }}
                        <button>Delete</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
