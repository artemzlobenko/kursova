
@extends('app.layouts.layout')
@section('page_title')
    <b>Trip info {{ $trip->city_name }}</b>
@endsection
@section('content')
    <p>City - {{ $trip->city_name }}</p>
    <p>Date - {{ $trip->date }}</p>
    <p>Tickets - {{ $trip->tickets }}/30</p>
    <p>Class - {{ $trip->class_type }}</p>
    <br/><br/>

    <form action="/trips/{{ $trip->trip_id }}" method="PATCH">
        <p>Tickets to buy:</p>
        <input type="number" name="book">
        <input type="submit" value="Book"/>
    </form>

    <a class="button" href="/trips">Back to tour list</a>
    <a class="button" href="/admin/trips">To admin list</a>
@endsection
