

@extends('app.layouts.layout')
@section('page_title')
    <b>Travel list</b>
@endsection
@section('content')
    <form method="get" action="/trips">
        <select name="dest">
            <option value="0">All categories</option>
            @foreach($trip_cities as $city)
                <option value="{{ $city->city_id }}"
                    {{ ( $city->city_id == $city_selected ) ? 'selected' : '' }}>
                    {{ $city->city_name }}
                </option>
            @endforeach
        </select>
        From:
        <input type="date" name="dateFrom" value="{{ $date_from_selected }}">
        To:
        <input type="date" name="dateTo" value="{{ $date_to_selected }}">
        in stock
        <input type="checkbox" name="inStock" {{ $in_stock ? 'checked' : ''}}>

        <input class="button" type="submit" value="Знайти">
    </form>

    <table style="text-align: center">
        <th> # </th>
        <th> Destination </th>
        <th> Date </th>
        <th> Tickets </th>
        <th> Class </th>
        @foreach ($trips as $trip)
            <tr>
                <td>
                    <a class="button" href="/trips/{{ $trip->trip_id }}">
                        {{ $trip->trip_id }}
                    </a>
                </td>
                <td>{{ $trip->city_name }}</td>
                <td>{{ $trip->date }}</td>
                <td>{{ $trip->tickets }}/30</td>
                <td>{{ $trip->class_type }}</td>
            </tr>
        @endforeach
    </table>
    <a class="button" href="/">Home</a>
@endsection
