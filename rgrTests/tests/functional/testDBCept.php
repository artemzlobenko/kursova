<?php

class testBDCept
{
    public function testDB(FunctionalTester $I)
    {
        $I->haveInDatabase('trips',
            array('trip_id' => '100', 'class_id' => '11',
                'driver_id' => '11', 'destination_id' => '11',
                'date' => '2020-09-27', 'tickets' => '11',
                'misca' => '12'));

        $I->seeInDatabase('trips', ['class_id' => '11']);
    }
}